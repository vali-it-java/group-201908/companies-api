package ee.valiit.companiesapi.repository;

import ee.valiit.companiesapi.model.Company;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;

@Repository
public class CompanyRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<Company> fetchCompanies() {
        List<Company> companies = jdbcTemplate.query(
                "SELECT * FROM company",
                (row, rowNum) -> {
                    return new Company(
                            row.getInt("id"),
                            row.getString("name"),
                            row.getString("logo")
                    );
                }
        );
        return companies;
    }

    public Company fetchCompany(int id) {
        List<Company> companies = jdbcTemplate.query(
                "SELECT * FROM company WHERE id = ?",
                new Object[] { id },
                (row, rowNum) -> {
                    return new Company(
                            row.getInt("id"),
                            row.getString("name"),
                            row.getString("logo")
                    );
                }
        );
//        return companies.size() > 0 ? companies.get(0) : null;
        if (companies.size() > 0) {
            return companies.get(0);
        } else {
            return null;
        }
    }

    public void deleteCompany(int id){
        jdbcTemplate.update("DELETE FROM company WHERE id = ?", id);
    }

    public void addCompany(Company company) {
        String name = company.getName();
        String logo = company.getLogo();

        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(connection -> {
                PreparedStatement ps =
                        connection.prepareStatement(
                                "INSERT INTO company (name, logo) VALUES (?, ?)",
                                Statement.RETURN_GENERATED_KEYS
                        );
                ps.setString(1, name);
                ps.setString(2, logo);
                return ps;
            },
            keyHolder
        );
        System.out.println(keyHolder.getKey().intValue());
        //jdbcTemplate.update("INSERT INTO company (name, logo) VALUES (?, ?)", name, logo);
    }

    public void updateCompany(Company company) {
        jdbcTemplate.update("UPDATE company SET name = ?, logo = ? WHERE id = ?",
                company.getName(), company.getLogo(), company.getId());
    }
}
