package ee.valiit.companiesapi.controller;

import ee.valiit.companiesapi.model.Company;
import ee.valiit.companiesapi.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("*")
public class CompaniesController {

    @Autowired
    private CompanyRepository companyRepository;

    @GetMapping("/companies")
    public List<Company> getCompanies() {
        return companyRepository.fetchCompanies();
    }

    @GetMapping("/company")
    public Company getCompany(@RequestParam("id") int id) {
        return companyRepository.fetchCompany(id);
    }

    @DeleteMapping("/company")
    public void deleteCompany(@RequestParam("id") int id) {
        companyRepository.deleteCompany(id);
    }

    @PostMapping("/company")
    public void addCompany(@RequestBody Company x) {
        companyRepository.addCompany(x);
    }

    @PutMapping("/company")
    public void updateCompany(@RequestBody Company y) {
        companyRepository.updateCompany(y);
    }
}
